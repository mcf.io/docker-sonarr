FROM mono:6.12-slim

# application dependencies, we add them here to support better layer caching.
# hadolint ignore=DL3008
RUN apt-get update && \
  apt-get install -yqq --no-install-recommends \
    libmono-microsoft-csharp4.0-cil \
    libmono-posix4.0-cil \
    libmono-system-net-http4.0-cil \
    libmono-system-componentmodel-dataannotations4.0-cil \
    libmono-system-configuration-install4.0-cil \
    libmono-system-data-datasetextensions4.0-cil \
    libmono-system-data4.0-cil \
    libmono-system-drawing4.0-cil \
    libmono-system-identitymodel4.0-cil \
    libmono-system-io-compression4.0-cil \
    libmono-system-runtime-serialization4.0-cil \
    libmono-system-servicemodel4.0a-cil \
    libmono-system-serviceprocess4.0-cil \
    libmono-system-transactions4.0-cil \
    libmono-system-web-extensions4.0-cil \
    libmono-system-web4.0-cil \
    libmono-system-xml-linq4.0-cil \
    curl \
    jq \
    mediainfo \
    sqlite3 \
  && rm -rf /var/lib/apt/lists/*

ARG CREATED
ARG SOURCE
ARG REVISION
ARG VERSION

# container metadata
LABEL org.opencontainers.image.title="mcf.io Sonarr - Smart PVR for newsgroup and bittorrent users." \
  org.opencontainers.image.url="https://sonarr.tv/" \
  org.opencontainers.image.created="${CREATED}" \
  org.opencontainers.image.source="${SOURCE}" \
  org.opencontainers.image.revision="${REVISION}"

ENV APP_PATH "/opt/sonarr"
ENV CONFIG_PATH "/config"
ENV BRANCH "main"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN mkdir -p "${APP_PATH}/bin" && \
  curl -fsSL "http://download.sonarr.tv/v3/${BRANCH}/${VERSION}/Sonarr.${BRANCH}.${VERSION}.linux.tar.gz" | tar xzf - -C "${APP_PATH}/bin" --strip-components=1 && \
  printf "UpdateMethod=docker\nBranch=%s\nPackageVersion=%s-%s\nPackageAuthor=[mcf.io](%s)" "${BRANCH}" "${VERSION}" "${REVISION}" "${SOURCE}" > "${APP_PATH}/package_info" && \
  rm -rf "${APP_PATH}/bin/Sonarr.Update" && \
  chmod --recursive u=rwX,go=rX "${APP_PATH}"

ENTRYPOINT [ "mono", "--debug", "/opt/sonarr/bin/Sonarr.exe" ]
CMD [ "-nobrowser", "-data=/config" ]
