# mcf.io/docker-sonarr

[Sonarr](https://sonarr.tv/) is a PVR for Usenet and BitTorrent users. It can monitor multiple RSS feeds for new episodes of your favorite shows and will grab, sort and rename them. It can also be configured to automatically upgrade the quality of files already downloaded when a better quality format becomes available.

## Usage

```shell
docker create \
  --name sonarr \
  -p 8989:8989 \
  -e PUID=<UID> -e PGID=<GID> \
  -v /etc/localtime:/etc/localtime \
  -v </path/to/appdata>:/config \
  -v </path/to/tvseries>:/tv \
  -v </path/to/downloads>:/downloads \
  mcfio/sonarr
```

## Parameters

Docker parameters are split into two components, separated by a colon, the left size represents the host and the right the container.  Example, the -p 8989:8989 parameter identifies the port mapping from host to container.

* `-p 8989:8989` - mapping host:container port for Sonarr web interface
* `-v </path/to/appdata>:/config` - Sonarr config and database directory
* `-v </path/to/tvseries>:/tv` - host volume mapping for TV library
* `-v </path/to/downloads>:/downloads` - host volume mapping for downloads

The container is based on the [mono:5.18-slim](https://hub.docker.com/_/mono/) image, which is debian stretch based, to gain access to the shell execute `docker exec -it sonarr bash`

### User / Group IDs

At times, the host volume permissions may not match those of IDs within the running container.  To resolve this the container allows specifying the `PUID` and `PGID`.  Ensure the host volume permissions match those of the container.

For example, either create or use an existing user on the host system and use the values for uid and gid.

```shell
$ id <docker user>
uid=1001(dockeruser) gid=1001(dockeruser) groups=1001(dockeruser)
```

Note: For added security this container supports the use of the `--user` flag both on command line and in `docker-compose` yaml.  The caveat is that ownership of the volume mounts must be set prior to starting the container and should match the values provided to `--user`.

## Info

You can monitor the logs for the container using `docker logs -f sonarr`.
